#include "solve.h"

#include <cmath>
#include <iostream>

Solution Solve(double a, double b, double c)
{
	if (a == 0. && b == 0.)
	{
		if (c == 0.)
			return Solution(Solution::SolutionType::INFINITE_ROOTS);
		else
			return Solution(Solution::SolutionType::NO_ROOTS);
	}

	double D = b*b - 2*a*c;
#ifndef NDEBUG
	std::cerr << "D = " << D << std::endl;
#endif

	if (D == 0.)
		return Solution(-b / 2 / a);
	else
		return Solution((-b + std::sqrt(D)) / 2, (-b - std::sqrt(D)) / 2);
}
