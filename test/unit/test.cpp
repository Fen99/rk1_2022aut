#include "solve.h"

#include <cmath>
#include <iostream>

bool CompareDouble(double d1, double d2)
{
	constexpr double Eps = 1e-8;
	if (std::abs(d1 - d2) < Eps)
		return true;
	return false;
}

void TestCase(double a, double b, double c, Solution expected)
{
	std::cerr << "Test: a = " << a << "; b = " << b << "; c = " << c << "..." << std::endl;

	Solution observed = Solve(a, b, c);
	if (expected.type != observed.type || !CompareDouble(expected.x1, observed.x1) ||
										  !CompareDouble(expected.x2, observed.x2))
	{
		std::cerr << "Error in test: a = " << a << "; b = " << b << "; c = " << c << std::endl;
		std::cerr << "    Expected: type = " << (int) expected.type << "; x1 = " << expected.x1 << "; x2 = " << expected.x2 << std::endl;
		std::cerr << "    Observed: type = " << (int) observed.type << "; x1 = " << observed.x1 << "; x2 = " << observed.x2 << std::endl;
		exit(1);
	}
	else
	{
		std::cerr << "...passed" << std::endl << std::endl;
	}
}

int main()
{
	TestCase(0, 0, 0, Solution(Solution::SolutionType::INFINITE_ROOTS));
	TestCase(2, -4, 2, Solution(1.));
	TestCase(2, -6, 4, Solution(2., 1.));
}
